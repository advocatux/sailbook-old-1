import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
            id: downloadDialog
            visible: false
            title: i18n.tr("Download compleed!")

            Text {
               // font.family: "Arial"
                font.pixelSize: 40
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr('Facebook dont open files, you can find them in: <br>"/home/phablet/.local/share/sailbook.sailbook/"<br> on your device.')
            }

            Button {
                text: i18n.tr('Close')
                onClicked: {
                    PopupUtils.close(downloadDialog);   
                    webview.goBack();
                    webview.goBack()
                }
            }
         }
